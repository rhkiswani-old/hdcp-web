package com.mrk.hdcp.web.util;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.commons.validator.UrlValidator;

@FacesValidator(value = "urlValidator")
public class URLValidator implements Validator{

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		UrlValidator urlValidator = new UrlValidator();
		if(!urlValidator.isValid(value.toString()) || value == null || value.toString() == null || !value.toString().endsWith("/")){
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_FATAL, "URL is invalid",null));
		}
	}

}

package com.mrk.hdcp.web.util;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 * 
 * @author mkiswani
 * 
 */
public class JsfUtil {

	public static void showError(String string) {
		System.out.println("================>Error Msg to View <===============================");
		System.out.println(string);
		FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_FATAL, string, null));
	}

	public static void showSucess(String string) {
		FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_INFO, string, null));
	}

	public static File getImage(String imageName) {
		String folderName = "/images";
		File externalFolder = getExternalFolder(folderName);
		File f = new File(externalFolder, imageName);
		return f;
	}

	public static File getExternalFolder(String folderName) {
		ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
		String realPath = servletContext.getRealPath(folderName);
		return new File(realPath);
	}

	public static void printReport(Map<String, Object> reportValues, String reportFile) {
		JasperReport compiledTemplate = null;
		JRExporter exporter = null;
		ByteArrayOutputStream out = null;
		ByteArrayInputStream pdfInputStream = null;
		BufferedOutputStream output = null;

		EntityManager entityManager = Persistence.createEntityManagerFactory("PU").createEntityManager();
		try {
			compiledTemplate = (JasperReport) JRLoader.loadObject(JsfUtil.class.getResourceAsStream(reportFile));
			out = new ByteArrayOutputStream();
			entityManager.getTransaction().begin();
			java.sql.Connection connection = entityManager.unwrap(java.sql.Connection.class);

			JasperPrint jasperPrint = JasperFillManager.fillReport(compiledTemplate, reportValues, connection);

			exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
			exporter.exportReport();

			pdfInputStream = new ByteArrayInputStream(out.toByteArray());
			FacesContext facesContext = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
			response.reset();
			response.setHeader("Content-Type", "application/pdf");
			OutputStream responseOutputStream = response.getOutputStream();
			byte[] bytesBuffer = new byte[2048];
			int bytesRead;
			while ((bytesRead = pdfInputStream.read(bytesBuffer)) > 0) {
				responseOutputStream.write(bytesBuffer, 0, bytesRead);
			}
			responseOutputStream.flush();
			pdfInputStream.close();
			responseOutputStream.close();
			facesContext.responseComplete();

		} catch (Exception e) {
			JsfUtil.showError(e.getMessage());
		} finally {
			try {
				entityManager.getTransaction().commit();
				if (output != null) {
					output.close();
				}
				if (pdfInputStream != null) {
					pdfInputStream.close();
				}
			} catch (Exception exception) {
				/* ... */
			}
		}
	}
}
package com.mrk.hdcp.web.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.mrk.hdcp.web.daos.CompaniesJpaController;
import com.mrk.hdcp.web.entites.Company;
import com.mrk.hdcp.web.util.JsfUtil;

@ManagedBean
@SessionScoped
public class CompaniesMB {

	private CompaniesJpaController companyJpaController = null;
	private Company selectedCompany;
	private Company company = new Company();
	private List<Company> companiesList = new ArrayList<Company>();
	private boolean selected;

	public CompaniesMB() {
		companyJpaController = new CompaniesJpaController();
		refershCompaniesList();
	}

	private void refershCompaniesList(){
		companiesList = companyJpaController.findCompaniesEntities();
	}
	
	public void selectCompany() {
		company = selectedCompany;
		selected = true;
	}

	
	public void saveCompany() {

		try {
			if (null == company.getCompanyId()) {
				companyJpaController.create(company);
			} else {
				companyJpaController.edit(company);
			}
			initCompany();
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.toString());
		}
	}

	public void removeCompany() {
		try {
			companyJpaController.destroy(selectedCompany.getCompanyId());
			initCompany();
		} catch (Exception e) {
			JsfUtil.showError(e.getMessage());
		}

	}

	public void initCompany() throws Exception {
		refershCompaniesList();
		company = new Company();
		selectedCompany = null;
	}

	public Company findCompany(int id) throws Exception {
		return companyJpaController.findCompanies(id);
	}

	public CompaniesJpaController getCompanyJpaController() {
		return companyJpaController;
	}

	public void setCompanyJpaController(CompaniesJpaController CompanyJpaController) {
		this.companyJpaController = CompanyJpaController;
	}

	public Company getSelectedCompany() {
		return selectedCompany;
	}

	public void setSelectedCompany(Company selectedCompany) {
		this.selectedCompany = selectedCompany;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public List<Company> getCompaniesList() {
		return companiesList;
	}

	public void setCompaniesList(List<Company> companiesList) {
		this.companiesList = companiesList;
	}

	
}
package com.mrk.hdcp.web.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.mrk.hdcp.web.daos.UsersJpaController;
import com.mrk.hdcp.web.daos.exceptions.NonexistentEntityException;
import com.mrk.hdcp.web.entites.User;
import com.mrk.hdcp.web.util.JsfUtil;

@ManagedBean
@ViewScoped
public class UsersMB {

	UsersJpaController userJpaController = null;
	private User selectUser;
	private User user = new User();
	private List<User> usersList = new ArrayList<User>();
	private String confirmPassword;
	private boolean selected;

	public UsersMB() {
		userJpaController = new UsersJpaController();
		refershUsersList();
	}

	private void refershUsersList() {
		usersList = userJpaController.findUsersEntities();
	}

	public void setSelectArticle(User selectUser) {
		this.selectUser = selectUser;
	}

	public User getSelectUser() {
		return selectUser;
	}

	public void setSelectUser(User selectUser) {
		this.selectUser = selectUser;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<User> getUsersList() {
		return usersList;
	}

	public void setUsersList(List<User> usersList) {
		this.usersList = usersList;
	}
	
	public void userSelect(){
//		/TODO
		user = selectUser;
		selected = true;
	}
	public void saveUser() {

		try {
			if (null == user.getUserId()) {
				userJpaController.create(user);
			} else {
				userJpaController.edit(user);
			}
			initUser();
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.toString());
		}
	}

	public void removeUser() {
		try {
			userJpaController.destroy(selectUser.getUserId());
		} catch (NonexistentEntityException e) {
			JsfUtil.showError(e.getMessage());
		}
		initUser();

	}

	public void initUser() {
		refershUsersList();
		user = new User();
		selectUser = null;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public User findUser(int id) {
		return userJpaController.findUsers(id);
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	
	
}
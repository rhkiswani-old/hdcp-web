package com.mrk.hdcp.web.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import com.mrk.hdcp.web.daos.CheckOutDetailsJpaController;
import com.mrk.hdcp.web.daos.PersonsJpaController;
import com.mrk.hdcp.web.daos.VehiclesJpaController;
import com.mrk.hdcp.web.entites.CheckInDetails;
import com.mrk.hdcp.web.entites.CheckOutDetails;
import com.mrk.hdcp.web.entites.Company;
import com.mrk.hdcp.web.entites.Person;
import com.mrk.hdcp.web.util.JsfUtil;

@ManagedBean
@ViewScoped
public class ReportsMB {

	private Company companyId;
	private String plateNumber;
	private String permanentNumber;
	private String personName;
	private Date fromDate;
	private Date toDate;
	private List<CheckOutDetails> companyCars = new ArrayList<CheckOutDetails>();
	private List<CheckOutDetails> companyPpl = new ArrayList<CheckOutDetails>();
	private CheckOutDetailsJpaController checkOutDetailsJpaController = new CheckOutDetailsJpaController();
	private String reportType = "";

	public ReportsMB() {
	}

	public void find() {
		boolean validate = validate();
		if(validate){
			if (reportType.equals("company")) {
				companyPpl = checkOutDetailsJpaController.findCompanyPplTransactions(companyId.getCompanyId(), fromDate, toDate);
				companyCars = checkOutDetailsJpaController.findCompanyCarsTransactions(companyId.getCompanyId(), fromDate, toDate);
			} else if (reportType.equals("car")) {
				companyCars = checkOutDetailsJpaController.findCarTransactions(plateNumber, permanentNumber, fromDate, toDate);
			} else if (reportType.equals("person")) {
				companyPpl = checkOutDetailsJpaController.findPersonTransactions(personName, fromDate, toDate);
			}
		}
	}

	private boolean validate() {
		if (reportType.equals("company")) {
			if (companyId == null) {
				JsfUtil.showError("Please Select Company");
				return false;
			}
			companyPpl = checkOutDetailsJpaController.findCompanyPplTransactions(companyId.getCompanyId(), fromDate, toDate);
			companyCars = checkOutDetailsJpaController.findCompanyCarsTransactions(companyId.getCompanyId(), fromDate, toDate);
		} else if (reportType.equals("car")) {
			if ((plateNumber == null || plateNumber.equals("")) && (permanentNumber == null || permanentNumber.equals(""))) {
				JsfUtil.showError("Please input numbers to find");
				companyCars = null;
				return false;
			}
			companyCars = checkOutDetailsJpaController.findCarTransactions(plateNumber, permanentNumber, fromDate, toDate);
		} else if (reportType.equals("person")) {
			if ((personName == null || personName.equals(""))) {
				JsfUtil.showError("Please input a name to serach");
				companyPpl = null;
				return false;
			}
			companyPpl = checkOutDetailsJpaController.findPersonTransactions(personName, fromDate, toDate);
		}
		return true;
	}

	public void print(){
		boolean validate = validate();
		if(validate){
			Map<String, Object> reportValues = new HashMap<String, Object>();
			reportValues.put("fromDate", new Timestamp(fromDate.getTime()));
			reportValues.put("toDate", new Timestamp(toDate.getTime()));
			if (reportType.equals("company")) {
				reportValues.put("companyId", getCompanyId().getCompanyId());
				JsfUtil.printReport(reportValues, "/company.jasper");
			} else if (reportType.equals("car")) {
				reportValues.put("vehiclePermementId", getPermanentNumber());
				reportValues.put("vehiclePlateNumber", getPlateNumber());
				JsfUtil.printReport(reportValues, "/car-report.jasper");
			} else if (reportType.equals("person")) {
				reportValues.put("personName", personName);
				JsfUtil.printReport(reportValues, "/person-report.jasper");
			}
		}
	}
	public void setReportType(String reportType) {
		this.reportType = reportType;
		companyCars = new ArrayList<CheckOutDetails>();
		companyPpl = new ArrayList<CheckOutDetails>();
	}

	public String getReportType() {
		return reportType;
	}

	public Company getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Company companyId) {
		this.companyId = companyId;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		if(toDate != null){
			Calendar c = Calendar.getInstance();
			c.setTime(toDate);
			c.set(Calendar.HOUR, 23);
			c.set(Calendar.MINUTE, 59);
			this.toDate = c.getTime();
		}
	}

	public List<CheckOutDetails> getCompanyCars() {
		return companyCars;
	}

	public void setCompanyCars(List<CheckOutDetails> companyCars) {
		this.companyCars = companyCars;
	}

	public List<CheckOutDetails> getCompanyPpl() {
		return companyPpl;
	}

	public void setCompanyPpl(List<CheckOutDetails> companyPpl) {
		this.companyPpl = companyPpl;
	}

	public String getPlateNumber() {
		return plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}

	public String getPermanentNumber() {
		return permanentNumber;
	}

	public void setPermanentNumber(String permanentNumber) {
		this.permanentNumber = permanentNumber;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}
	
	public List<String> personNameAutoComplete(String query){
		PersonsJpaController controller = new PersonsJpaController();
		List<String> findPersonNames = controller.findPersonNames();
		List<String> names = new ArrayList<String>();
		if(query == null || query.equals("")){
			return findPersonNames;
		}
		for (String name : findPersonNames) {
			if(name.toLowerCase().contains(query.toLowerCase())){
				names.add(name);
			}
		}
		return names;
	}

	public List<String> platenumberAutoComplete(String query){
		VehiclesJpaController controller = new VehiclesJpaController();
		List<String> plate = controller.findPlatenumbers();
		List<String> names = new ArrayList<String>();
		if(query == null || query.equals("")){
			return plate;
		}
		for (String name : plate) {
			if(name.toLowerCase().contains(query.toLowerCase())){
				names.add(name);
			}
		}
		return names;
	}
	
	public List<String> vehiclePermementAutoComplete(String query){
		VehiclesJpaController controller = new VehiclesJpaController();
		List<String> plate = controller.findVehiclePermementId();
		List<String> names = new ArrayList<String>();
		if(query == null || query.equals("")){
			return plate;
		}
		for (String name : plate) {
			if(name.toLowerCase().contains(query.toLowerCase())){
				names.add(name);
			}
		}
		return names;
	}
}
package com.mrk.hdcp.web.bean;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.mrk.hdcp.web.daos.VehiclesJpaController;
import com.mrk.hdcp.web.entites.Vehicle;
import com.mrk.hdcp.web.util.JsfUtil;

@ManagedBean
@SessionScoped
public class VechiclesMB {

	private VehiclesJpaController vehicleJpaController = null;
	private Vehicle selectedVehicle;
	private Vehicle vehicle = new Vehicle();
	private List<Vehicle> vehiclesList = new ArrayList<Vehicle>();
	private boolean selected;
	private String barcode;

	public VechiclesMB() {
		vehicleJpaController = new VehiclesJpaController();
		refershVehiclesList();
	}

	private void refershVehiclesList(){
		vehiclesList = vehicleJpaController.findVehiclesEntities();
	}
	
	public void printJasper() {
		Map<String, Object> reportValues = new HashMap<String, Object>();
		reportValues.put("vehicleId", getSelectedVehicle().getVehicleId());
		JsfUtil.printReport(reportValues, "/cards/card-cars.jasper");
	}
	
	public void vehicleSelect() {
		vehicle = selectedVehicle;
		selected = true;
	}

	
	public StreamedContent getImageStream() throws IOException {
		if(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("vehicleForm:barcode") == null || FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("vehicleForm:barcode").equals("null")){
			return null;
		}
		int id = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("vehicleForm:barcode"));
		return vehicleJpaController.findVehicles(id).getImage();
	}
	public void upload(FileUploadEvent event) {
		UploadedFile file = event.getFile();
		if (file != null) {
			InputStream inputstream;
			try {
				inputstream = file.getInputstream();
				if (inputstream != null) {

					byte[] buffer = new byte[inputstream.available()];
					IOUtils.read(inputstream, buffer);
					selectedVehicle.setVehicle(buffer);
					if(selectedVehicle.getVehicleId() != null && selectedVehicle.getVehicleId() != 0){
						saveVehicle();
					}
					JsfUtil.showSucess(file.getFileName() + " is uploaded.");
				}
			} catch (Exception e) {
				JsfUtil.showError("Failed to upload due to : " + e.getMessage());
			}
		} else {
			JsfUtil.showError("Please add file");
		}
	}

	public void saveVehicle() {

		try {
			if (null == vehicle.getVehicleId()) {
				vehicleJpaController.create(vehicle);
			} else {
				vehicleJpaController.edit(vehicle);
			}
			initVehicle();
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.toString());
		}
	}

	public void removeVehicle() {
		try {
			vehicleJpaController.destroy(selectedVehicle.getVehicleId());
			initVehicle();
		} catch (Exception e) {
			JsfUtil.showError(e.getMessage());
		}

	}

	public void initVehicle() throws Exception {
		refershVehiclesList();
		vehicle = new Vehicle();
		selectedVehicle = null;
	}

	public void inputChanged(AjaxBehaviorEvent event) {
		String obj =  (String) ((UIOutput)event.getSource()).getValue();
		if(obj != null ){
			try{
				int id = Integer.parseInt(obj);
				Vehicle vehicle = vehicleJpaController.findVehicles(id);
				this.selectedVehicle = vehicle;
			}catch(Exception e ){
				
			}
		}
		System.out.println(obj);
	}

	public void checkIn() {
//		if(getSelectedVehicle() != null){
//			if(isCarParked()){
//				throw new IllegalArgumentException("Car already parked");
//			}
//			CheckInOutDetails details = new CheckInOutDetails();
//			details.setCheckinDate(new Date());
//			details.setInTheParking(true);
//			details.setVehicles(getSelectedVehicle());
//			checkInOutDetailsJpaController.create(details);
//		}
		System.out.println(barcode);
	}

	
	public boolean isCarParked() {
//		return checkInOutDetailsJpaController.isCarParked(getSelectedVehicle().getCarId());
		return false;
	}


	public void checkOut() {
//		if(getSelectedVehicle() != null){
//			if(!isCarParked()){
//				throw new IllegalArgumentException("Car is not parked");
//			}
//			CheckInOutDetails details = checkInOutDetailsJpaController.getCarParked(getSelectedVehicle().getCarId());
//			details.setCheckoutDate(new Date());
//			details.setInTheParking(false);
//			try {
//				checkInOutDetailsJpaController.edit(details);
//			} catch (Exception e) {
//				JsfUtil.showError("Failed to Checkout due to : "+ e.getMessage());
//			}
//		}
		System.out.println(barcode);
	}

	public Vehicle findVehicle(int id) throws Exception {
		return vehicleJpaController.findVehicles(id);
	}

	public VehiclesJpaController getVehicleJpaController() {
		return vehicleJpaController;
	}

	public void setVehicleJpaController(VehiclesJpaController vehicleJpaController) {
		this.vehicleJpaController = vehicleJpaController;
	}

	public Vehicle getSelectedVehicle() {
		return selectedVehicle;
	}

	public void setSelectedVehicle(Vehicle selectedVehicle) {
		this.selectedVehicle = selectedVehicle;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public List<Vehicle> getVehiclesList() {
		return vehiclesList;
	}

	public void setVehiclesList(List<Vehicle> vehiclesList) {
		this.vehiclesList = vehiclesList;
	}

	
}
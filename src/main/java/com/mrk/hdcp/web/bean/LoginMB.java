package com.mrk.hdcp.web.bean;

import java.io.IOException;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.mrk.hdcp.web.daos.UsersJpaController;
import com.mrk.hdcp.web.entites.User;
import com.mrk.hdcp.web.util.JsfUtil;

/**
 * 
 * @author mkiswani
 *
 */
@SessionScoped
@ManagedBean
public class LoginMB {

	private String username;
	private String password;
	private boolean vaildUser;

	public String login() {
		UsersJpaController userJpaController = new UsersJpaController();
		try {
			List<User> findUsersEntities = userJpaController.findUsersEntities();
			User foundedUser = null;
			for (User user : findUsersEntities) {
				if (user.getEmail() != null && user.getEmail().equals(username) && user.getPassword() != null && user.getPassword().equals(password)) {
					foundedUser = user;
					break;
				}
			}
			if (foundedUser == null) {
				throw new RuntimeException("Invalid username or password");
			}
			if (foundedUser.getIsDriver()) {
				throw new RuntimeException("Your account is defined as Driver you are not allowed to login");
			}
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("loggedInUser", foundedUser);
			vaildUser = true;
			FacesContext.getCurrentInstance().getExternalContext().redirect("home.xhtml");
			return null;
		} catch (Exception e) {
			JsfUtil.showError(e.getMessage());
			return "index.xhtml";
		}
	}

	public String logout() {
		vaildUser = false;
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("loggedInUser", null);
		} catch (IOException e) {
		}
		return null;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isVaildUser() {
		return vaildUser;
	}

	public void setVaildUser(boolean vaildUser) {
		this.vaildUser = vaildUser;
	}

}

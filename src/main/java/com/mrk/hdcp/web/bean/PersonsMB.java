package com.mrk.hdcp.web.bean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.mrk.hdcp.web.daos.PersonsJpaController;
import com.mrk.hdcp.web.entites.Person;
import com.mrk.hdcp.web.util.JsfUtil;

@ManagedBean
@SessionScoped
public class PersonsMB {

	private PersonsJpaController personJpaController = null;
	private Person selectedPerson;
	private Person person = new Person();
	private List<Person> personsList = new ArrayList<Person>();
	private boolean selected;

	public PersonsMB() {
		personJpaController = new PersonsJpaController();
		refershPersonsList();
	}

	public void printJasper() {
		Map<String, Object> reportValues = new HashMap<String, Object>();
		reportValues.put("personId", getSelectedPerson().getPersonId());
		JsfUtil.printReport(reportValues, "/cards/card-person.jasper");
	}

	public String getBarCodeURL() {
		if (getSelectedPerson() == null || getSelectedPerson().getPersonId() == null) {
			return "";
		}
		return "http://www.barcodesinc.com/generator/image.php?code=P-" + getSelectedPerson().getPersonId() + "&type=C128B&width=200&height=50&xres=1&font=3";
	}

	private void refershPersonsList() {
		personsList = personJpaController.findPersonEntities();
	}

	public void selectPerson() {
		person = selectedPerson;
		selected = true;
	}

	public void upload(FileUploadEvent event) {
		UploadedFile file = event.getFile();
		if (file != null) {
			InputStream inputstream;
			try {
				inputstream = file.getInputstream();
				if (inputstream != null) {

					byte[] buffer = new byte[inputstream.available()];
					IOUtils.read(inputstream, buffer);
					selectedPerson.setPersonPicture(buffer);
					if(selectedPerson.getPersonId() != null && selectedPerson.getPersonId() != 0){
						savePerson();
					}
					JsfUtil.showSucess(file.getFileName() + " is uploaded.");
				}
			} catch (Exception e) {
				JsfUtil.showError("Failed to upload due to : " + e.getMessage());
			}
		} else {
			JsfUtil.showError("Please add file");
		}
	}

	public void savePerson() {

		try {
			if (null == person.getPersonId()) {
				personJpaController.create(person);
			} else {
				personJpaController.edit(person);
			}
			initPerson();
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.toString());
		}
	}

	public void removePerson() {
		try {
			personJpaController.destroy(selectedPerson.getPersonId());
			initPerson();
		} catch (Exception e) {
			JsfUtil.showError(e.getMessage());
		}

	}

	public void initPerson() throws Exception {
		refershPersonsList();
		person = new Person();
		selectedPerson = null;
	}

	public Person findPerson(int id) throws Exception {
		return personJpaController.findPerson(id);
	}

	public PersonsJpaController getPersonJpaController() {
		return personJpaController;
	}

	public void setPersonJpaController(PersonsJpaController PersonJpaController) {
		this.personJpaController = PersonJpaController;
	}

	public Person getSelectedPerson() {
		return selectedPerson;
	}

	public void setSelectedPerson(Person selectedPerson) {
		this.selectedPerson = selectedPerson;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public List<Person> getPersonsList() {
		return personsList;
	}

	public void setPersonsList(List<Person> personsList) {
		this.personsList = personsList;
	}

	public StreamedContent getImage() throws IOException {
		if (selectedPerson != null && selectedPerson.getPersonPicture() != null) {
			return new DefaultStreamedContent(new ByteArrayInputStream(selectedPerson.getPersonPicture()));
		}
		return null;
	}
}
package com.mrk.hdcp.web.bean;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.neo4j.cypher.internal.compiler.v2_1.functions.Timestamp;

import com.mrk.hdcp.web.daos.CheckInDetailsJpaController;
import com.mrk.hdcp.web.daos.CheckOutDetailsJpaController;
import com.mrk.hdcp.web.daos.PersonsJpaController;
import com.mrk.hdcp.web.daos.VehiclesJpaController;
import com.mrk.hdcp.web.entites.CheckInDetails;
import com.mrk.hdcp.web.entites.CheckOutDetails;
import com.mrk.hdcp.web.entites.Person;
import com.mrk.hdcp.web.entites.Vehicle;
import com.mrk.hdcp.web.util.JsfUtil;

@ManagedBean
@javax.faces.bean.SessionScoped
public class BarcodeReaderMB {

	private Person foundPerson = null;
	private Vehicle foundVehicle = null;
	private PersonsJpaController personsJpaController = new PersonsJpaController();
	private VehiclesJpaController vehiclesJpaController = new VehiclesJpaController();
	private CheckInDetailsJpaController checkInDetailsJpaController = new CheckInDetailsJpaController();
	private CheckOutDetailsJpaController checkOutDetailsJpaController = new CheckOutDetailsJpaController();
	private String notes = "";
	private String barcode;
	private Vector<CheckInDetails> checkedInCars = new Vector<CheckInDetails>();
	private Vector<CheckInDetails> checkedInPeople = new Vector<CheckInDetails>();
	private boolean found = false;

	public BarcodeReaderMB() {
		refershLists();
	}

	private void refershLists() {
		checkedInCars = checkInDetailsJpaController.getCheckInCars();
		checkedInPeople = checkInDetailsJpaController.getCheckInPeople();
		foundPerson = null;
		foundVehicle = null;
		barcode ="";
		notes = "";
	}
	
	public void printDailyReport(){
		Map<String, Object> reportValues = new HashMap<String, Object>();
		reportValues.put("companyId", -1);
		Date d = new  Date();
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.set(Calendar.HOUR, 0);
		reportValues.put("fromDate", new java.sql.Timestamp(c.getTimeInMillis()));
		c.set(Calendar.HOUR, 23);
		c.set(Calendar.MINUTE, 59);
		reportValues.put("toDate", new java.sql.Timestamp(c.getTimeInMillis()));
		
		JsfUtil.printReport(reportValues, "/daily.jasper");
	}

	public void serach() {
		if (barcode == null) {
			return;
		}
		if (barcode.startsWith("P-")) {
			foundPerson = personsJpaController.findPerson(Integer.parseInt(barcode.split("P-")[1]));
			foundVehicle = null;
			return;
		}
		if (barcode.startsWith("-")) {
			foundVehicle = vehiclesJpaController.findVehicles(Integer.parseInt(barcode.split("-")[1]));
			foundPerson = null;
			return;
		}
		JsfUtil.showError("Barcode Not Found");
		barcode = "";
	}

	public void checkInIntoParking() {
		if(isCheckedIn()){
			JsfUtil.showError("Already checked in ");
			return;
		}
		CheckInDetails checkInDetails = new CheckInDetails();
		checkInDetails.setCheckInDate(new Date());
		checkInDetails.setInTheParking(true);
		checkInDetails.setNotes(notes);
		if (foundPerson != null) {
			checkInDetails.setPersonId(foundPerson);
		}
		if (foundVehicle != null) {
			checkInDetails.setVehicleId(foundVehicle);
		}
		if(checkInDetails.getPersonId() != null || checkInDetails.getVehicleId() != null){
			checkInDetailsJpaController.create(checkInDetails);
			refershLists();
			JsfUtil.showSucess("Done ");
		}

	}

	public void checkOut() {
		if(!isCheckedIn()){
			JsfUtil.showError("Already checked out ");
			return;
		}
		try {
			EntityManager entityManager = checkOutDetailsJpaController.getEntityManager();
			EntityTransaction transaction = entityManager.getTransaction();
			transaction.begin();
			CheckInDetails checkIn = null;
			if (barcode.startsWith("P-")) {
				checkIn = checkInDetailsJpaController.findCheckInDetailsByPersonId(Integer.parseInt(barcode.split("P-")[1]));
				checkIn.setInTheParking(false);
				checkInDetailsJpaController.edit(checkIn);
			}
			if (barcode.startsWith("-")) {
				checkIn = checkInDetailsJpaController.findCheckInDetailsByVehicleId(Integer.parseInt(barcode.split("-")[1]));
				checkIn.setInTheParking(false);
				checkInDetailsJpaController.edit(checkIn);
			}
			if(checkIn != null){
				CheckOutDetails checkOutDetails = new CheckOutDetails();
				checkOutDetails.setCheckOutDate(new Date());
				checkOutDetails.setNotes(getNotes());
				checkOutDetails.setCheckInId(checkIn.getCheckInId());
				checkOutDetails.setPersonId(foundPerson);
				checkOutDetails.setVehicleId(foundVehicle);
				checkOutDetailsJpaController.create(checkOutDetails);
			}
			transaction.commit();
		} catch (Exception e) {
			JsfUtil.showError(e.getMessage());
			return;
		}
		refershLists();
		JsfUtil.showSucess("Done ");
	}
	
	
	public Person getFoundPerson() {
		return foundPerson;
	}

	public void setFoundPerson(Person foundPerson) {
		this.foundPerson = foundPerson;
	}

	public Vehicle getFoundVehicle() {
		return foundVehicle;
	}

	public void setFoundVehicle(Vehicle foundVehicle) {
		this.foundVehicle = foundVehicle;
	}

	public PersonsJpaController getPersonsJpaController() {
		return personsJpaController;
	}

	public void setPersonsJpaController(PersonsJpaController personsJpaController) {
		this.personsJpaController = personsJpaController;
	}

	public VehiclesJpaController getVehiclesJpaController() {
		return vehiclesJpaController;
	}

	public void setVehiclesJpaController(VehiclesJpaController vehiclesJpaController) {
		this.vehiclesJpaController = vehiclesJpaController;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public boolean isCheckedIn() {
		if (barcode == null || barcode.equals("")) {
			return false;
		}
		if (barcode.startsWith("P-")) {
			CheckInDetails checkInDetails = checkInDetailsJpaController.findCheckInDetailsByPersonId(Integer.parseInt(barcode.split("P-")[1]));
			return checkInDetails != null;
		}
		if (barcode.startsWith("-")) {
			CheckInDetails checkInDetails = checkInDetailsJpaController.findCheckInDetailsByVehicleId(Integer.parseInt(barcode.split("-")[1]));
			return checkInDetails != null;
		}
		return false;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public CheckInDetailsJpaController getCheckInDetailsJpaController() {
		return checkInDetailsJpaController;
	}

	public void setCheckInDetailsJpaController(CheckInDetailsJpaController checkInDetailsJpaController) {
		this.checkInDetailsJpaController = checkInDetailsJpaController;
	}

	public CheckOutDetailsJpaController getCheckOutDetailsJpaController() {
		return checkOutDetailsJpaController;
	}

	public void setCheckOutDetailsJpaController(CheckOutDetailsJpaController checkOutDetailsJpaController) {
		this.checkOutDetailsJpaController = checkOutDetailsJpaController;
	}

	public Vector<CheckInDetails> getCheckedInCars() {
		return checkedInCars;
	}

	public void setCheckedInCars(Vector<CheckInDetails> checkedInCars) {
		this.checkedInCars = checkedInCars;
	}

	public Vector<CheckInDetails> getCheckedInPeople() {
		return checkedInPeople;
	}

	public void setCheckedInPeople(Vector<CheckInDetails> checkedInPeople) {
		this.checkedInPeople = checkedInPeople;
	}

	public boolean isFound() {
		return found;
	}

	public void setFound(boolean found) {
		this.found = found;
	}

	
}

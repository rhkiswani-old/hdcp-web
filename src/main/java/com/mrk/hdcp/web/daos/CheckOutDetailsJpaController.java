/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mrk.hdcp.web.daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mrk.hdcp.web.daos.exceptions.NonexistentEntityException;
import com.mrk.hdcp.web.entites.CheckInDetails;
import com.mrk.hdcp.web.entites.CheckOutDetails;
import com.mrk.hdcp.web.entites.Person;
import com.mrk.hdcp.web.entites.Vehicle;

/**
 *
 * @author mkiswani
 */
public class CheckOutDetailsJpaController implements Serializable {

    public CheckOutDetailsJpaController() {
    	this.emf = Persistence.createEntityManagerFactory("PU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CheckOutDetails checkOutDetails) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Vehicle vehicleId = checkOutDetails.getVehicleId();
            if (vehicleId != null) {
                vehicleId = em.getReference(vehicleId.getClass(), vehicleId.getVehicleId());
                checkOutDetails.setVehicleId(vehicleId);
            }
            Person personId = checkOutDetails.getPersonId();
            if (personId != null) {
                personId = em.getReference(personId.getClass(), personId.getPersonId());
                checkOutDetails.setPersonId(personId);
            }
            em.persist(checkOutDetails);
            if (vehicleId != null) {
                vehicleId.getCheckOutDetailsCollection().add(checkOutDetails);
                vehicleId = em.merge(vehicleId);
            }
            if (personId != null) {
                personId.getCheckOutDetailsCollection().add(checkOutDetails);
                personId = em.merge(personId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CheckOutDetails checkOutDetails) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CheckOutDetails persistentCheckOutDetails = em.find(CheckOutDetails.class, checkOutDetails.getCheckOutId());
            Vehicle vehicleIdOld = persistentCheckOutDetails.getVehicleId();
            Vehicle vehicleIdNew = checkOutDetails.getVehicleId();
            Person personIdOld = persistentCheckOutDetails.getPersonId();
            Person personIdNew = checkOutDetails.getPersonId();
            if (vehicleIdNew != null) {
                vehicleIdNew = em.getReference(vehicleIdNew.getClass(), vehicleIdNew.getVehicleId());
                checkOutDetails.setVehicleId(vehicleIdNew);
            }
            if (personIdNew != null) {
                personIdNew = em.getReference(personIdNew.getClass(), personIdNew.getPersonId());
                checkOutDetails.setPersonId(personIdNew);
            }
            checkOutDetails = em.merge(checkOutDetails);
            if (vehicleIdOld != null && !vehicleIdOld.equals(vehicleIdNew)) {
                vehicleIdOld.getCheckOutDetailsCollection().remove(checkOutDetails);
                vehicleIdOld = em.merge(vehicleIdOld);
            }
            if (vehicleIdNew != null && !vehicleIdNew.equals(vehicleIdOld)) {
                vehicleIdNew.getCheckOutDetailsCollection().add(checkOutDetails);
                vehicleIdNew = em.merge(vehicleIdNew);
            }
            if (personIdOld != null && !personIdOld.equals(personIdNew)) {
                personIdOld.getCheckOutDetailsCollection().remove(checkOutDetails);
                personIdOld = em.merge(personIdOld);
            }
            if (personIdNew != null && !personIdNew.equals(personIdOld)) {
                personIdNew.getCheckOutDetailsCollection().add(checkOutDetails);
                personIdNew = em.merge(personIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = checkOutDetails.getCheckOutId();
                if (findCheckOutDetails(id) == null) {
                    throw new NonexistentEntityException("The checkOutDetails with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CheckOutDetails checkOutDetails;
            try {
                checkOutDetails = em.getReference(CheckOutDetails.class, id);
                checkOutDetails.getCheckOutId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The checkOutDetails with id " + id + " no longer exists.", enfe);
            }
            Vehicle vehicleId = checkOutDetails.getVehicleId();
            if (vehicleId != null) {
                vehicleId.getCheckOutDetailsCollection().remove(checkOutDetails);
                vehicleId = em.merge(vehicleId);
            }
            Person personId = checkOutDetails.getPersonId();
            if (personId != null) {
                personId.getCheckOutDetailsCollection().remove(checkOutDetails);
                personId = em.merge(personId);
            }
            em.remove(checkOutDetails);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CheckOutDetails> findCheckOutDetailsEntities() {
        return findCheckOutDetailsEntities(true, -1, -1);
    }

    public List<CheckOutDetails> findCheckOutDetailsEntities(int maxResults, int firstResult) {
        return findCheckOutDetailsEntities(false, maxResults, firstResult);
    }

    private List<CheckOutDetails> findCheckOutDetailsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CheckOutDetails.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CheckOutDetails findCheckOutDetails(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CheckOutDetails.class, id);
        } finally {
            em.close();
        }
    }

    public int getCheckOutDetailsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CheckOutDetails> rt = cq.from(CheckOutDetails.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

	public Vector<CheckOutDetails> findCompanyPplTransactions(Integer companyId, Date fromDate, Date toDate) {
		String fieldType = "personId";
		return findTransactions(companyId, fromDate, toDate, fieldType);
	}

	public Vector<CheckOutDetails> findCompanyCarsTransactions(Integer companyId, Date fromDate, Date toDate) {
		String fieldType = "vehicleId";
		return findTransactions(companyId, fromDate, toDate, fieldType);
	}

	protected Vector<CheckOutDetails> findTransactions(Integer companyId, Date fromDate, Date toDate, String fieldType) {
		EntityManager em = getEntityManager();
		try {
			Query createQuery = em.createQuery("Select u from CheckOutDetails u where u." + fieldType
					+ ".companyId.companyId=:companyId and u.checkOutDate >=:fromDate and u.checkOutDate <=:toDate", CheckOutDetails.class);
			createQuery.setParameter("companyId", companyId);
			createQuery.setParameter("fromDate", fromDate);
			createQuery.setParameter("toDate", toDate);
			createQuery.setHint("javax.persistence.cache.storeMode", "REFRESH");
			Object singleResult2 = createQuery.getResultList();
			if (singleResult2 != null) {
				Vector<CheckOutDetails> singleResult = (Vector<CheckOutDetails>) singleResult2;
				return singleResult;
			} 
			return null;
		} catch (NoResultException e) {
			return null;
		} finally {
			em.close();
		}
	}

	public List<CheckOutDetails> findCarTransactions(String vehiclePlateNumber, String vehiclePermementId, Date fromDate, Date toDate) {
		EntityManager em = getEntityManager();
		try {
			Query createQuery = em.createQuery("Select u from CheckOutDetails u where (u.vehicleId.vehiclePlateNumber=:vehiclePlateNumber or u.vehicleId.vehiclePermementId=:vehiclePermementId)and u.checkOutDate >=:fromDate and u.checkOutDate <=:toDate", CheckOutDetails.class);
			createQuery.setParameter("vehiclePlateNumber", vehiclePlateNumber);
			createQuery.setParameter("vehiclePermementId", vehiclePermementId);
			createQuery.setParameter("fromDate", fromDate);
			createQuery.setParameter("toDate", toDate);
			createQuery.setHint("javax.persistence.cache.storeMode", "REFRESH");
			Object singleResult2 = createQuery.getResultList();
			if (singleResult2 != null) {
				Vector<CheckOutDetails> singleResult = (Vector<CheckOutDetails>) singleResult2;
				return singleResult;
			} 
			return null;
		} catch (NoResultException e) {
			return null;
		} finally {
			em.close();
		}
	}

	public List<CheckOutDetails> findPersonTransactions(String personName, Date fromDate, Date toDate) {
		EntityManager em = getEntityManager();
		try {
			Query createQuery = em.createQuery("Select u from CheckOutDetails u where u.personId.personName=:personName and u.checkOutDate >=:fromDate and u.checkOutDate <=:toDate", CheckOutDetails.class);
			createQuery.setParameter("personName", personName);
			createQuery.setParameter("fromDate", fromDate);
			createQuery.setParameter("toDate", toDate);
			createQuery.setHint("javax.persistence.cache.storeMode", "REFRESH");
			Object singleResult2 = createQuery.getResultList();
			if (singleResult2 != null) {
				Vector<CheckOutDetails> singleResult = (Vector<CheckOutDetails>) singleResult2;
				return singleResult;
			} 
			return null;
		} catch (NoResultException e) {
			return null;
		} finally {
			em.close();
		}
	}
}


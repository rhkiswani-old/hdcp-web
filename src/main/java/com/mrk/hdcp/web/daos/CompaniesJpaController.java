/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mrk.hdcp.web.daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mrk.hdcp.web.daos.exceptions.NonexistentEntityException;
import com.mrk.hdcp.web.entites.Company;
import com.mrk.hdcp.web.entites.Vehicle;

/**
 *
 * @author mkiswani
 */
public class CompaniesJpaController implements Serializable {

    public CompaniesJpaController() {
    	this.emf = Persistence.createEntityManagerFactory("PU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Company companies) {
        if (companies.getVehiclesCollection() == null) {
            companies.setVehiclesCollection(new ArrayList<Vehicle>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Vehicle> attachedVehiclesCollection = new ArrayList<Vehicle>();
            for (Vehicle vehiclesCollectionVehiclesToAttach : companies.getVehiclesCollection()) {
                vehiclesCollectionVehiclesToAttach = em.getReference(vehiclesCollectionVehiclesToAttach.getClass(), vehiclesCollectionVehiclesToAttach.getVehicleId());
                attachedVehiclesCollection.add(vehiclesCollectionVehiclesToAttach);
            }
            companies.setVehiclesCollection(attachedVehiclesCollection);
            em.persist(companies);
            for (Vehicle vehiclesCollectionVehicles : companies.getVehiclesCollection()) {
                Company oldCompanyIdOfVehiclesCollectionVehicles = vehiclesCollectionVehicles.getCompanyId();
                vehiclesCollectionVehicles.setCompanyId(companies);
                vehiclesCollectionVehicles = em.merge(vehiclesCollectionVehicles);
                if (oldCompanyIdOfVehiclesCollectionVehicles != null) {
                    oldCompanyIdOfVehiclesCollectionVehicles.getVehiclesCollection().remove(vehiclesCollectionVehicles);
                    oldCompanyIdOfVehiclesCollectionVehicles = em.merge(oldCompanyIdOfVehiclesCollectionVehicles);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Company companies) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Company persistentCompanies = em.find(Company.class, companies.getCompanyId());
            Collection<Vehicle> vehiclesCollectionOld = persistentCompanies.getVehiclesCollection();
            Collection<Vehicle> vehiclesCollectionNew = companies.getVehiclesCollection();
            Collection<Vehicle> attachedVehiclesCollectionNew = new ArrayList<Vehicle>();
            for (Vehicle vehiclesCollectionNewVehiclesToAttach : vehiclesCollectionNew) {
                vehiclesCollectionNewVehiclesToAttach = em.getReference(vehiclesCollectionNewVehiclesToAttach.getClass(), vehiclesCollectionNewVehiclesToAttach.getVehicleId());
                attachedVehiclesCollectionNew.add(vehiclesCollectionNewVehiclesToAttach);
            }
            vehiclesCollectionNew = attachedVehiclesCollectionNew;
            companies.setVehiclesCollection(vehiclesCollectionNew);
            companies = em.merge(companies);
            for (Vehicle vehiclesCollectionOldVehicles : vehiclesCollectionOld) {
                if (!vehiclesCollectionNew.contains(vehiclesCollectionOldVehicles)) {
                    vehiclesCollectionOldVehicles.setCompanyId(null);
                    vehiclesCollectionOldVehicles = em.merge(vehiclesCollectionOldVehicles);
                }
            }
            for (Vehicle vehiclesCollectionNewVehicles : vehiclesCollectionNew) {
                if (!vehiclesCollectionOld.contains(vehiclesCollectionNewVehicles)) {
                    Company oldCompanyIdOfVehiclesCollectionNewVehicles = vehiclesCollectionNewVehicles.getCompanyId();
                    vehiclesCollectionNewVehicles.setCompanyId(companies);
                    vehiclesCollectionNewVehicles = em.merge(vehiclesCollectionNewVehicles);
                    if (oldCompanyIdOfVehiclesCollectionNewVehicles != null && !oldCompanyIdOfVehiclesCollectionNewVehicles.equals(companies)) {
                        oldCompanyIdOfVehiclesCollectionNewVehicles.getVehiclesCollection().remove(vehiclesCollectionNewVehicles);
                        oldCompanyIdOfVehiclesCollectionNewVehicles = em.merge(oldCompanyIdOfVehiclesCollectionNewVehicles);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = companies.getCompanyId();
                if (findCompanies(id) == null) {
                    throw new NonexistentEntityException("The companies with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Company companies;
            try {
                companies = em.getReference(Company.class, id);
                companies.getCompanyId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The companies with id " + id + " no longer exists.", enfe);
            }
            Collection<Vehicle> vehiclesCollection = companies.getVehiclesCollection();
            for (Vehicle vehiclesCollectionVehicles : vehiclesCollection) {
                vehiclesCollectionVehicles.setCompanyId(null);
                vehiclesCollectionVehicles = em.merge(vehiclesCollectionVehicles);
            }
            em.remove(companies);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Company> findCompaniesEntities() {
        return findCompaniesEntities(true, -1, -1);
    }

    public List<Company> findCompaniesEntities(int maxResults, int firstResult) {
        return findCompaniesEntities(false, maxResults, firstResult);
    }

    private List<Company> findCompaniesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Company.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Company findCompanies(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Company.class, id);
        } finally {
            em.close();
        }
    }

    public int getCompaniesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Company> rt = cq.from(Company.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

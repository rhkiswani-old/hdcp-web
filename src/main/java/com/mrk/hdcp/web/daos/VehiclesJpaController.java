/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mrk.hdcp.web.daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mrk.hdcp.web.daos.exceptions.NonexistentEntityException;
import com.mrk.hdcp.web.entites.CheckInDetails;
import com.mrk.hdcp.web.entites.CheckOutDetails;
import com.mrk.hdcp.web.entites.Company;
import com.mrk.hdcp.web.entites.Vehicle;

/**
 *
 * @author mkiswani
 */
public class VehiclesJpaController implements Serializable {

    public VehiclesJpaController() {
    	this.emf = Persistence.createEntityManagerFactory("PU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Vehicle vehicles) {
        if (vehicles.getCheckInDetailsCollection() == null) {
            vehicles.setCheckInDetailsCollection(new ArrayList<CheckInDetails>());
        }
        if (vehicles.getCheckOutDetailsCollection() == null) {
            vehicles.setCheckOutDetailsCollection(new ArrayList<CheckOutDetails>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Company companyId = vehicles.getCompanyId();
            if (companyId != null) {
                companyId = em.getReference(companyId.getClass(), companyId.getCompanyId());
                vehicles.setCompanyId(companyId);
            }
            Collection<CheckInDetails> attachedCheckInDetailsCollection = new ArrayList<CheckInDetails>();
            for (CheckInDetails checkInDetailsCollectionCheckInDetailsToAttach : vehicles.getCheckInDetailsCollection()) {
                checkInDetailsCollectionCheckInDetailsToAttach = em.getReference(checkInDetailsCollectionCheckInDetailsToAttach.getClass(), checkInDetailsCollectionCheckInDetailsToAttach.getCheckInId());
                attachedCheckInDetailsCollection.add(checkInDetailsCollectionCheckInDetailsToAttach);
            }
            vehicles.setCheckInDetailsCollection(attachedCheckInDetailsCollection);
            Collection<CheckOutDetails> attachedCheckOutDetailsCollection = new ArrayList<CheckOutDetails>();
            for (CheckOutDetails checkOutDetailsCollectionCheckOutDetailsToAttach : vehicles.getCheckOutDetailsCollection()) {
                checkOutDetailsCollectionCheckOutDetailsToAttach = em.getReference(checkOutDetailsCollectionCheckOutDetailsToAttach.getClass(), checkOutDetailsCollectionCheckOutDetailsToAttach.getCheckOutId());
                attachedCheckOutDetailsCollection.add(checkOutDetailsCollectionCheckOutDetailsToAttach);
            }
            vehicles.setCheckOutDetailsCollection(attachedCheckOutDetailsCollection);
            em.persist(vehicles);
            if (companyId != null) {
                companyId.getVehiclesCollection().add(vehicles);
                companyId = em.merge(companyId);
            }
            for (CheckInDetails checkInDetailsCollectionCheckInDetails : vehicles.getCheckInDetailsCollection()) {
                Vehicle oldVehicleIdOfCheckInDetailsCollectionCheckInDetails = checkInDetailsCollectionCheckInDetails.getVehicleId();
                checkInDetailsCollectionCheckInDetails.setVehicleId(vehicles);
                checkInDetailsCollectionCheckInDetails = em.merge(checkInDetailsCollectionCheckInDetails);
                if (oldVehicleIdOfCheckInDetailsCollectionCheckInDetails != null) {
                    oldVehicleIdOfCheckInDetailsCollectionCheckInDetails.getCheckInDetailsCollection().remove(checkInDetailsCollectionCheckInDetails);
                    oldVehicleIdOfCheckInDetailsCollectionCheckInDetails = em.merge(oldVehicleIdOfCheckInDetailsCollectionCheckInDetails);
                }
            }
            for (CheckOutDetails checkOutDetailsCollectionCheckOutDetails : vehicles.getCheckOutDetailsCollection()) {
                Vehicle oldVehicleIdOfCheckOutDetailsCollectionCheckOutDetails = checkOutDetailsCollectionCheckOutDetails.getVehicleId();
                checkOutDetailsCollectionCheckOutDetails.setVehicleId(vehicles);
                checkOutDetailsCollectionCheckOutDetails = em.merge(checkOutDetailsCollectionCheckOutDetails);
                if (oldVehicleIdOfCheckOutDetailsCollectionCheckOutDetails != null) {
                    oldVehicleIdOfCheckOutDetailsCollectionCheckOutDetails.getCheckOutDetailsCollection().remove(checkOutDetailsCollectionCheckOutDetails);
                    oldVehicleIdOfCheckOutDetailsCollectionCheckOutDetails = em.merge(oldVehicleIdOfCheckOutDetailsCollectionCheckOutDetails);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Vehicle vehicles) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Vehicle persistentVehicles = em.find(Vehicle.class, vehicles.getVehicleId());
            Company companyIdOld = persistentVehicles.getCompanyId();
            Company companyIdNew = vehicles.getCompanyId();
            Collection<CheckInDetails> checkInDetailsCollectionOld = persistentVehicles.getCheckInDetailsCollection();
            Collection<CheckInDetails> checkInDetailsCollectionNew = vehicles.getCheckInDetailsCollection();
            Collection<CheckOutDetails> checkOutDetailsCollectionOld = persistentVehicles.getCheckOutDetailsCollection();
            Collection<CheckOutDetails> checkOutDetailsCollectionNew = vehicles.getCheckOutDetailsCollection();
            if (companyIdNew != null) {
                companyIdNew = em.getReference(companyIdNew.getClass(), companyIdNew.getCompanyId());
                vehicles.setCompanyId(companyIdNew);
            }
            Collection<CheckInDetails> attachedCheckInDetailsCollectionNew = new ArrayList<CheckInDetails>();
            for (CheckInDetails checkInDetailsCollectionNewCheckInDetailsToAttach : checkInDetailsCollectionNew) {
                checkInDetailsCollectionNewCheckInDetailsToAttach = em.getReference(checkInDetailsCollectionNewCheckInDetailsToAttach.getClass(), checkInDetailsCollectionNewCheckInDetailsToAttach.getCheckInId());
                attachedCheckInDetailsCollectionNew.add(checkInDetailsCollectionNewCheckInDetailsToAttach);
            }
            checkInDetailsCollectionNew = attachedCheckInDetailsCollectionNew;
            vehicles.setCheckInDetailsCollection(checkInDetailsCollectionNew);
            Collection<CheckOutDetails> attachedCheckOutDetailsCollectionNew = new ArrayList<CheckOutDetails>();
            for (CheckOutDetails checkOutDetailsCollectionNewCheckOutDetailsToAttach : checkOutDetailsCollectionNew) {
                checkOutDetailsCollectionNewCheckOutDetailsToAttach = em.getReference(checkOutDetailsCollectionNewCheckOutDetailsToAttach.getClass(), checkOutDetailsCollectionNewCheckOutDetailsToAttach.getCheckOutId());
                attachedCheckOutDetailsCollectionNew.add(checkOutDetailsCollectionNewCheckOutDetailsToAttach);
            }
            checkOutDetailsCollectionNew = attachedCheckOutDetailsCollectionNew;
            vehicles.setCheckOutDetailsCollection(checkOutDetailsCollectionNew);
            vehicles = em.merge(vehicles);
            if (companyIdOld != null && !companyIdOld.equals(companyIdNew)) {
                companyIdOld.getVehiclesCollection().remove(vehicles);
                companyIdOld = em.merge(companyIdOld);
            }
            if (companyIdNew != null && !companyIdNew.equals(companyIdOld)) {
                companyIdNew.getVehiclesCollection().add(vehicles);
                companyIdNew = em.merge(companyIdNew);
            }
            for (CheckInDetails checkInDetailsCollectionOldCheckInDetails : checkInDetailsCollectionOld) {
                if (!checkInDetailsCollectionNew.contains(checkInDetailsCollectionOldCheckInDetails)) {
                    checkInDetailsCollectionOldCheckInDetails.setVehicleId(null);
                    checkInDetailsCollectionOldCheckInDetails = em.merge(checkInDetailsCollectionOldCheckInDetails);
                }
            }
            for (CheckInDetails checkInDetailsCollectionNewCheckInDetails : checkInDetailsCollectionNew) {
                if (!checkInDetailsCollectionOld.contains(checkInDetailsCollectionNewCheckInDetails)) {
                    Vehicle oldVehicleIdOfCheckInDetailsCollectionNewCheckInDetails = checkInDetailsCollectionNewCheckInDetails.getVehicleId();
                    checkInDetailsCollectionNewCheckInDetails.setVehicleId(vehicles);
                    checkInDetailsCollectionNewCheckInDetails = em.merge(checkInDetailsCollectionNewCheckInDetails);
                    if (oldVehicleIdOfCheckInDetailsCollectionNewCheckInDetails != null && !oldVehicleIdOfCheckInDetailsCollectionNewCheckInDetails.equals(vehicles)) {
                        oldVehicleIdOfCheckInDetailsCollectionNewCheckInDetails.getCheckInDetailsCollection().remove(checkInDetailsCollectionNewCheckInDetails);
                        oldVehicleIdOfCheckInDetailsCollectionNewCheckInDetails = em.merge(oldVehicleIdOfCheckInDetailsCollectionNewCheckInDetails);
                    }
                }
            }
            for (CheckOutDetails checkOutDetailsCollectionOldCheckOutDetails : checkOutDetailsCollectionOld) {
                if (!checkOutDetailsCollectionNew.contains(checkOutDetailsCollectionOldCheckOutDetails)) {
                    checkOutDetailsCollectionOldCheckOutDetails.setVehicleId(null);
                    checkOutDetailsCollectionOldCheckOutDetails = em.merge(checkOutDetailsCollectionOldCheckOutDetails);
                }
            }
            for (CheckOutDetails checkOutDetailsCollectionNewCheckOutDetails : checkOutDetailsCollectionNew) {
                if (!checkOutDetailsCollectionOld.contains(checkOutDetailsCollectionNewCheckOutDetails)) {
                    Vehicle oldVehicleIdOfCheckOutDetailsCollectionNewCheckOutDetails = checkOutDetailsCollectionNewCheckOutDetails.getVehicleId();
                    checkOutDetailsCollectionNewCheckOutDetails.setVehicleId(vehicles);
                    checkOutDetailsCollectionNewCheckOutDetails = em.merge(checkOutDetailsCollectionNewCheckOutDetails);
                    if (oldVehicleIdOfCheckOutDetailsCollectionNewCheckOutDetails != null && !oldVehicleIdOfCheckOutDetailsCollectionNewCheckOutDetails.equals(vehicles)) {
                        oldVehicleIdOfCheckOutDetailsCollectionNewCheckOutDetails.getCheckOutDetailsCollection().remove(checkOutDetailsCollectionNewCheckOutDetails);
                        oldVehicleIdOfCheckOutDetailsCollectionNewCheckOutDetails = em.merge(oldVehicleIdOfCheckOutDetailsCollectionNewCheckOutDetails);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = vehicles.getVehicleId();
                if (findVehicles(id) == null) {
                    throw new NonexistentEntityException("The vehicles with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Vehicle vehicles;
            try {
                vehicles = em.getReference(Vehicle.class, id);
                vehicles.getVehicleId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The vehicles with id " + id + " no longer exists.", enfe);
            }
            Company companyId = vehicles.getCompanyId();
            if (companyId != null) {
                companyId.getVehiclesCollection().remove(vehicles);
                companyId = em.merge(companyId);
            }
            Collection<CheckInDetails> checkInDetailsCollection = vehicles.getCheckInDetailsCollection();
            for (CheckInDetails checkInDetailsCollectionCheckInDetails : checkInDetailsCollection) {
                checkInDetailsCollectionCheckInDetails.setVehicleId(null);
                checkInDetailsCollectionCheckInDetails = em.merge(checkInDetailsCollectionCheckInDetails);
            }
            Collection<CheckOutDetails> checkOutDetailsCollection = vehicles.getCheckOutDetailsCollection();
            for (CheckOutDetails checkOutDetailsCollectionCheckOutDetails : checkOutDetailsCollection) {
                checkOutDetailsCollectionCheckOutDetails.setVehicleId(null);
                checkOutDetailsCollectionCheckOutDetails = em.merge(checkOutDetailsCollectionCheckOutDetails);
            }
            em.remove(vehicles);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Vehicle> findVehiclesEntities() {
        return findVehiclesEntities(true, -1, -1);
    }

    public List<Vehicle> findVehiclesEntities(int maxResults, int firstResult) {
        return findVehiclesEntities(false, maxResults, firstResult);
    }

    private List<Vehicle> findVehiclesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Vehicle.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Vehicle findVehicles(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Vehicle.class, id);
        } finally {
            em.close();
        }
    }

    public int getVehiclesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Vehicle> rt = cq.from(Vehicle.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<String> findPlatenumbers() {
		EntityManager em = getEntityManager();
		try {
			Query createQuery = em.createQuery("Select u.vehiclePlateNumber from Vehicle u ", String.class);
			createQuery.setHint("javax.persistence.cache.storeMode", "REFRESH");
			Object singleResult2 = createQuery.getResultList();
			if (singleResult2 != null) {
				Vector<String> singleResult = (Vector<String>) singleResult2;
				return singleResult;
			} 
			return null;
		} catch (NoResultException e) {
			return null;
		} finally {
			em.close();
		}
	}
    
    public List<String> findVehiclePermementId() {
		EntityManager em = getEntityManager();
		try {
			Query createQuery = em.createQuery("Select u.vehiclePermementId from Vehicle u ", String.class);
			createQuery.setHint("javax.persistence.cache.storeMode", "REFRESH");
			Object singleResult2 = createQuery.getResultList();
			if (singleResult2 != null) {
				Vector<String> singleResult = (Vector<String>) singleResult2;
				return singleResult;
			} 
			return null;
		} catch (NoResultException e) {
			return null;
		} finally {
			em.close();
		}
	}
}

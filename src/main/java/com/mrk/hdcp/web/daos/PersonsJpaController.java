/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mrk.hdcp.web.daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mrk.hdcp.web.daos.exceptions.NonexistentEntityException;
import com.mrk.hdcp.web.daos.exceptions.PreexistingEntityException;
import com.mrk.hdcp.web.entites.CheckInDetails;
import com.mrk.hdcp.web.entites.CheckOutDetails;
import com.mrk.hdcp.web.entites.Company;
import com.mrk.hdcp.web.entites.Person;

/**
 *
 * @author mkiswani
 */
public class PersonsJpaController implements Serializable {

    public PersonsJpaController() {
        this.emf = Persistence.createEntityManagerFactory("PU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Person persons) throws PreexistingEntityException, Exception {
        if (persons.getCheckInDetailsCollection() == null) {
            persons.setCheckInDetailsCollection(new ArrayList<CheckInDetails>());
        }
        if (persons.getCheckOutDetailsCollection() == null) {
            persons.setCheckOutDetailsCollection(new ArrayList<CheckOutDetails>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Company companyId = persons.getCompanyId();
            if (companyId != null) {
                companyId = em.getReference(companyId.getClass(), companyId.getCompanyId());
                persons.setCompanyId(companyId);
            }
            Collection<CheckInDetails> attachedCheckInDetailsCollection = new ArrayList<CheckInDetails>();
            for (CheckInDetails checkInDetailsCollectionCheckInDetailsToAttach : persons.getCheckInDetailsCollection()) {
                checkInDetailsCollectionCheckInDetailsToAttach = em.getReference(checkInDetailsCollectionCheckInDetailsToAttach.getClass(), checkInDetailsCollectionCheckInDetailsToAttach.getCheckInId());
                attachedCheckInDetailsCollection.add(checkInDetailsCollectionCheckInDetailsToAttach);
            }
            persons.setCheckInDetailsCollection(attachedCheckInDetailsCollection);
            Collection<CheckOutDetails> attachedCheckOutDetailsCollection = new ArrayList<CheckOutDetails>();
            for (CheckOutDetails checkOutDetailsCollectionCheckOutDetailsToAttach : persons.getCheckOutDetailsCollection()) {
                checkOutDetailsCollectionCheckOutDetailsToAttach = em.getReference(checkOutDetailsCollectionCheckOutDetailsToAttach.getClass(), checkOutDetailsCollectionCheckOutDetailsToAttach.getCheckOutId());
                attachedCheckOutDetailsCollection.add(checkOutDetailsCollectionCheckOutDetailsToAttach);
            }
            persons.setCheckOutDetailsCollection(attachedCheckOutDetailsCollection);
            em.persist(persons);
            if (companyId != null) {
                companyId.getPersonsCollection().add(persons);
                companyId = em.merge(companyId);
            }
            for (CheckInDetails checkInDetailsCollectionCheckInDetails : persons.getCheckInDetailsCollection()) {
                Person oldPersonIdOfCheckInDetailsCollectionCheckInDetails = checkInDetailsCollectionCheckInDetails.getPersonId();
                checkInDetailsCollectionCheckInDetails.setPersonId(persons);
                checkInDetailsCollectionCheckInDetails = em.merge(checkInDetailsCollectionCheckInDetails);
                if (oldPersonIdOfCheckInDetailsCollectionCheckInDetails != null) {
                    oldPersonIdOfCheckInDetailsCollectionCheckInDetails.getCheckInDetailsCollection().remove(checkInDetailsCollectionCheckInDetails);
                    oldPersonIdOfCheckInDetailsCollectionCheckInDetails = em.merge(oldPersonIdOfCheckInDetailsCollectionCheckInDetails);
                }
            }
            for (CheckOutDetails checkOutDetailsCollectionCheckOutDetails : persons.getCheckOutDetailsCollection()) {
                Person oldPersonIdOfCheckOutDetailsCollectionCheckOutDetails = checkOutDetailsCollectionCheckOutDetails.getPersonId();
                checkOutDetailsCollectionCheckOutDetails.setPersonId(persons);
                checkOutDetailsCollectionCheckOutDetails = em.merge(checkOutDetailsCollectionCheckOutDetails);
                if (oldPersonIdOfCheckOutDetailsCollectionCheckOutDetails != null) {
                    oldPersonIdOfCheckOutDetailsCollectionCheckOutDetails.getCheckOutDetailsCollection().remove(checkOutDetailsCollectionCheckOutDetails);
                    oldPersonIdOfCheckOutDetailsCollectionCheckOutDetails = em.merge(oldPersonIdOfCheckOutDetailsCollectionCheckOutDetails);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPerson(persons.getPersonId()) != null) {
                throw new PreexistingEntityException("Person " + persons + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Person persons) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Person persistentPerson = em.find(Person.class, persons.getPersonId());
            Company companyIdOld = persistentPerson.getCompanyId();
            Company companyIdNew = persons.getCompanyId();
            Collection<CheckInDetails> checkInDetailsCollectionOld = persistentPerson.getCheckInDetailsCollection();
            Collection<CheckInDetails> checkInDetailsCollectionNew = persons.getCheckInDetailsCollection();
            Collection<CheckOutDetails> checkOutDetailsCollectionOld = persistentPerson.getCheckOutDetailsCollection();
            Collection<CheckOutDetails> checkOutDetailsCollectionNew = persons.getCheckOutDetailsCollection();
            if (companyIdNew != null) {
                companyIdNew = em.getReference(companyIdNew.getClass(), companyIdNew.getCompanyId());
                persons.setCompanyId(companyIdNew);
            }
            Collection<CheckInDetails> attachedCheckInDetailsCollectionNew = new ArrayList<CheckInDetails>();
            for (CheckInDetails checkInDetailsCollectionNewCheckInDetailsToAttach : checkInDetailsCollectionNew) {
                checkInDetailsCollectionNewCheckInDetailsToAttach = em.getReference(checkInDetailsCollectionNewCheckInDetailsToAttach.getClass(), checkInDetailsCollectionNewCheckInDetailsToAttach.getCheckInId());
                attachedCheckInDetailsCollectionNew.add(checkInDetailsCollectionNewCheckInDetailsToAttach);
            }
            checkInDetailsCollectionNew = attachedCheckInDetailsCollectionNew;
            persons.setCheckInDetailsCollection(checkInDetailsCollectionNew);
            Collection<CheckOutDetails> attachedCheckOutDetailsCollectionNew = new ArrayList<CheckOutDetails>();
            for (CheckOutDetails checkOutDetailsCollectionNewCheckOutDetailsToAttach : checkOutDetailsCollectionNew) {
                checkOutDetailsCollectionNewCheckOutDetailsToAttach = em.getReference(checkOutDetailsCollectionNewCheckOutDetailsToAttach.getClass(), checkOutDetailsCollectionNewCheckOutDetailsToAttach.getCheckOutId());
                attachedCheckOutDetailsCollectionNew.add(checkOutDetailsCollectionNewCheckOutDetailsToAttach);
            }
            checkOutDetailsCollectionNew = attachedCheckOutDetailsCollectionNew;
            persons.setCheckOutDetailsCollection(checkOutDetailsCollectionNew);
            persons = em.merge(persons);
            if (companyIdOld != null && !companyIdOld.equals(companyIdNew)) {
                companyIdOld.getPersonsCollection().remove(persons);
                companyIdOld = em.merge(companyIdOld);
            }
            if (companyIdNew != null && !companyIdNew.equals(companyIdOld)) {
                companyIdNew.getPersonsCollection().add(persons);
                companyIdNew = em.merge(companyIdNew);
            }
            for (CheckInDetails checkInDetailsCollectionOldCheckInDetails : checkInDetailsCollectionOld) {
                if (!checkInDetailsCollectionNew.contains(checkInDetailsCollectionOldCheckInDetails)) {
                    checkInDetailsCollectionOldCheckInDetails.setPersonId(null);
                    checkInDetailsCollectionOldCheckInDetails = em.merge(checkInDetailsCollectionOldCheckInDetails);
                }
            }
            for (CheckInDetails checkInDetailsCollectionNewCheckInDetails : checkInDetailsCollectionNew) {
                if (!checkInDetailsCollectionOld.contains(checkInDetailsCollectionNewCheckInDetails)) {
                    Person oldPersonIdOfCheckInDetailsCollectionNewCheckInDetails = checkInDetailsCollectionNewCheckInDetails.getPersonId();
                    checkInDetailsCollectionNewCheckInDetails.setPersonId(persons);
                    checkInDetailsCollectionNewCheckInDetails = em.merge(checkInDetailsCollectionNewCheckInDetails);
                    if (oldPersonIdOfCheckInDetailsCollectionNewCheckInDetails != null && !oldPersonIdOfCheckInDetailsCollectionNewCheckInDetails.equals(persons)) {
                        oldPersonIdOfCheckInDetailsCollectionNewCheckInDetails.getCheckInDetailsCollection().remove(checkInDetailsCollectionNewCheckInDetails);
                        oldPersonIdOfCheckInDetailsCollectionNewCheckInDetails = em.merge(oldPersonIdOfCheckInDetailsCollectionNewCheckInDetails);
                    }
                }
            }
            for (CheckOutDetails checkOutDetailsCollectionOldCheckOutDetails : checkOutDetailsCollectionOld) {
                if (!checkOutDetailsCollectionNew.contains(checkOutDetailsCollectionOldCheckOutDetails)) {
                    checkOutDetailsCollectionOldCheckOutDetails.setPersonId(null);
                    checkOutDetailsCollectionOldCheckOutDetails = em.merge(checkOutDetailsCollectionOldCheckOutDetails);
                }
            }
            for (CheckOutDetails checkOutDetailsCollectionNewCheckOutDetails : checkOutDetailsCollectionNew) {
                if (!checkOutDetailsCollectionOld.contains(checkOutDetailsCollectionNewCheckOutDetails)) {
                    Person oldPersonIdOfCheckOutDetailsCollectionNewCheckOutDetails = checkOutDetailsCollectionNewCheckOutDetails.getPersonId();
                    checkOutDetailsCollectionNewCheckOutDetails.setPersonId(persons);
                    checkOutDetailsCollectionNewCheckOutDetails = em.merge(checkOutDetailsCollectionNewCheckOutDetails);
                    if (oldPersonIdOfCheckOutDetailsCollectionNewCheckOutDetails != null && !oldPersonIdOfCheckOutDetailsCollectionNewCheckOutDetails.equals(persons)) {
                        oldPersonIdOfCheckOutDetailsCollectionNewCheckOutDetails.getCheckOutDetailsCollection().remove(checkOutDetailsCollectionNewCheckOutDetails);
                        oldPersonIdOfCheckOutDetailsCollectionNewCheckOutDetails = em.merge(oldPersonIdOfCheckOutDetailsCollectionNewCheckOutDetails);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = persons.getPersonId();
                if (findPerson(id) == null) {
                    throw new NonexistentEntityException("The persons with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Person persons;
            try {
                persons = em.getReference(Person.class, id);
                persons.getPersonId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The persons with id " + id + " no longer exists.", enfe);
            }
            Company companyId = persons.getCompanyId();
            if (companyId != null) {
                companyId.getPersonsCollection().remove(persons);
                companyId = em.merge(companyId);
            }
            Collection<CheckInDetails> checkInDetailsCollection = persons.getCheckInDetailsCollection();
            for (CheckInDetails checkInDetailsCollectionCheckInDetails : checkInDetailsCollection) {
                checkInDetailsCollectionCheckInDetails.setPersonId(null);
                checkInDetailsCollectionCheckInDetails = em.merge(checkInDetailsCollectionCheckInDetails);
            }
            Collection<CheckOutDetails> checkOutDetailsCollection = persons.getCheckOutDetailsCollection();
            for (CheckOutDetails checkOutDetailsCollectionCheckOutDetails : checkOutDetailsCollection) {
                checkOutDetailsCollectionCheckOutDetails.setPersonId(null);
                checkOutDetailsCollectionCheckOutDetails = em.merge(checkOutDetailsCollectionCheckOutDetails);
            }
            em.remove(persons);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Person> findPersonEntities() {
        return findPersonEntities(true, -1, -1);
    }

    public List<Person> findPersonEntities(int maxResults, int firstResult) {
        return findPersonEntities(false, maxResults, firstResult);
    }

    private List<Person> findPersonEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Person.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Person findPerson(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Person.class, id);
        } finally {
            em.close();
        }
    }

    public int getPersonCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Person> rt = cq.from(Person.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<String> findPersonNames() {
		EntityManager em = getEntityManager();
		try {
			Query createQuery = em.createQuery("Select u.personName from Person u ", String.class);
			createQuery.setHint("javax.persistence.cache.storeMode", "REFRESH");
			Object singleResult2 = createQuery.getResultList();
			if (singleResult2 != null) {
				Vector<String> singleResult = (Vector<String>) singleResult2;
				return singleResult;
			} 
			return null;
		} catch (NoResultException e) {
			return null;
		} finally {
			em.close();
		}
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mrk.hdcp.web.daos;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mrk.hdcp.web.daos.exceptions.NonexistentEntityException;
import com.mrk.hdcp.web.entites.User;

/**
 *
 * @author mkiswani
 */
public class UsersJpaController implements Serializable {

	public UsersJpaController() {
		this.emf = Persistence.createEntityManagerFactory("PU");
	}

	private EntityManagerFactory emf = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mrk.hdcp.web.daos.JpaController#getEntityManager()
	 */
	
	public EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mrk.hdcp.web.daos.JpaController#create(com.mrk.hdcp.web.entites.User)
	 */
	
	public void create(User users) {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			em.persist(users);
			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mrk.hdcp.web.daos.JpaController#edit(com.mrk.hdcp.web.entites.User)
	 */
	
	public void edit(User users) throws NonexistentEntityException, Exception {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			users = em.merge(users);
			em.getTransaction().commit();
		} catch (Exception ex) {
			String msg = ex.getLocalizedMessage();
			if (msg == null || msg.length() == 0) {
				Integer id = users.getUserId();
				if (findUsers(id) == null) {
					throw new NonexistentEntityException("The users with id " + id + " no longer exists.");
				}
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mrk.hdcp.web.daos.JpaController#destroy(java.lang.Integer)
	 */
	
	public void destroy(Integer id) throws NonexistentEntityException {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			User users;
			try {
				users = em.getReference(User.class, id);
				users.getUserId();
			} catch (EntityNotFoundException enfe) {
				throw new NonexistentEntityException("The users with id " + id + " no longer exists.", enfe);
			}
			em.remove(users);
			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mrk.hdcp.web.daos.JpaController#findUsersEntities()
	 */
	
	public List<User> findUsersEntities() {
		return findUsersEntities(true, -1, -1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mrk.hdcp.web.daos.JpaController#findUsersEntities(int, int)
	 */
	
	public List<User> findUsersEntities(int maxResults, int firstResult) {
		return findUsersEntities(false, maxResults, firstResult);
	}

	private List<User> findUsersEntities(boolean all, int maxResults, int firstResult) {
		EntityManager em = getEntityManager();
		try {
			CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
			cq.select(cq.from(User.class));
			Query q = em.createQuery(cq);
			if (!all) {
				q.setMaxResults(maxResults);
				q.setFirstResult(firstResult);
			}
			return q.getResultList();
		} finally {
			em.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mrk.hdcp.web.daos.JpaController#findUsers(java.lang.Integer)
	 */
	
	public User findUsers(Integer id) {
		EntityManager em = getEntityManager();
		try {
			return em.find(User.class, id);
		} finally {
			em.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mrk.hdcp.web.daos.JpaController#getUsersCount()
	 */
	
	public int getUsersCount() {
		EntityManager em = getEntityManager();
		try {
			CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
			Root<User> rt = cq.from(User.class);
			cq.select(em.getCriteriaBuilder().count(rt));
			Query q = em.createQuery(cq);
			return ((Long) q.getSingleResult()).intValue();
		} finally {
			em.close();
		}
	}

}

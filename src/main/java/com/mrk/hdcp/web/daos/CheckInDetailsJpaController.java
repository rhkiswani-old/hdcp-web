/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mrk.hdcp.web.daos;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mrk.hdcp.web.daos.exceptions.NonexistentEntityException;
import com.mrk.hdcp.web.entites.CheckInDetails;
import com.mrk.hdcp.web.entites.Person;
import com.mrk.hdcp.web.entites.Vehicle;

/**
 *
 * @author mkiswani
 */
public class CheckInDetailsJpaController implements Serializable {

    public CheckInDetailsJpaController() {
    	this.emf = Persistence.createEntityManagerFactory("PU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CheckInDetails checkInDetails) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Person personId = checkInDetails.getPersonId();
            if (personId != null) {
                personId = em.getReference(personId.getClass(), personId.getPersonId());
                checkInDetails.setPersonId(personId);
            }
            Vehicle vehicleId = checkInDetails.getVehicleId();
            if (vehicleId != null) {
                vehicleId = em.getReference(vehicleId.getClass(), vehicleId.getVehicleId());
                checkInDetails.setVehicleId(vehicleId);
            }
            em.persist(checkInDetails);
            if (personId != null) {
                personId.getCheckInDetailsCollection().add(checkInDetails);
                personId = em.merge(personId);
            }
            if (vehicleId != null) {
                vehicleId.getCheckInDetailsCollection().add(checkInDetails);
                vehicleId = em.merge(vehicleId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CheckInDetails checkInDetails) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CheckInDetails persistentCheckInDetails = em.find(CheckInDetails.class, checkInDetails.getCheckInId());
            Person personIdOld = persistentCheckInDetails.getPersonId();
            Person personIdNew = checkInDetails.getPersonId();
            Vehicle vehicleIdOld = persistentCheckInDetails.getVehicleId();
            Vehicle vehicleIdNew = checkInDetails.getVehicleId();
            if (personIdNew != null) {
                personIdNew = em.getReference(personIdNew.getClass(), personIdNew.getPersonId());
                checkInDetails.setPersonId(personIdNew);
            }
            if (vehicleIdNew != null) {
                vehicleIdNew = em.getReference(vehicleIdNew.getClass(), vehicleIdNew.getVehicleId());
                checkInDetails.setVehicleId(vehicleIdNew);
            }
            checkInDetails = em.merge(checkInDetails);
            if (personIdOld != null && !personIdOld.equals(personIdNew)) {
                personIdOld.getCheckInDetailsCollection().remove(checkInDetails);
                personIdOld = em.merge(personIdOld);
            }
            if (personIdNew != null && !personIdNew.equals(personIdOld)) {
                personIdNew.getCheckInDetailsCollection().add(checkInDetails);
                personIdNew = em.merge(personIdNew);
            }
            if (vehicleIdOld != null && !vehicleIdOld.equals(vehicleIdNew)) {
                vehicleIdOld.getCheckInDetailsCollection().remove(checkInDetails);
                vehicleIdOld = em.merge(vehicleIdOld);
            }
            if (vehicleIdNew != null && !vehicleIdNew.equals(vehicleIdOld)) {
                vehicleIdNew.getCheckInDetailsCollection().add(checkInDetails);
                vehicleIdNew = em.merge(vehicleIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = checkInDetails.getCheckInId();
                if (findCheckInDetails(id) == null) {
                    throw new NonexistentEntityException("The checkInDetails with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CheckInDetails checkInDetails;
            try {
                checkInDetails = em.getReference(CheckInDetails.class, id);
                checkInDetails.getCheckInId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The checkInDetails with id " + id + " no longer exists.", enfe);
            }
            Person personId = checkInDetails.getPersonId();
            if (personId != null) {
                personId.getCheckInDetailsCollection().remove(checkInDetails);
                personId = em.merge(personId);
            }
            Vehicle vehicleId = checkInDetails.getVehicleId();
            if (vehicleId != null) {
                vehicleId.getCheckInDetailsCollection().remove(checkInDetails);
                vehicleId = em.merge(vehicleId);
            }
            em.remove(checkInDetails);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CheckInDetails> findCheckInDetailsEntities() {
        return findCheckInDetailsEntities(true, -1, -1);
    }

    public List<CheckInDetails> findCheckInDetailsEntities(int maxResults, int firstResult) {
        return findCheckInDetailsEntities(false, maxResults, firstResult);
    }

    private List<CheckInDetails> findCheckInDetailsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CheckInDetails.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CheckInDetails findCheckInDetails(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CheckInDetails.class, id);
        } finally {
            em.close();
        }
    }

    public int getCheckInDetailsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CheckInDetails> rt = cq.from(CheckInDetails.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

	public CheckInDetails findCheckInDetailsByPersonId(int personId) {
		EntityManager em = getEntityManager();
		try {
			Query createQuery = em.createQuery("Select u from CheckInDetails u where u.personId.personId=:personId and u.inTheParking = true", CheckInDetails.class);
			createQuery.setParameter("personId", personId);
			createQuery.setHint("javax.persistence.cache.storeMode", "REFRESH");
			Object singleResult2 = createQuery.getSingleResult();
			if (singleResult2 != null) {
				CheckInDetails singleResult = (CheckInDetails) singleResult2;
				return singleResult;
			} 
			return null;
		} catch (NoResultException e) {
			return null;
		} finally {
			em.close();
		}
	}

	public CheckInDetails findCheckInDetailsByVehicleId(int vehicleId) {
		EntityManager em = getEntityManager();
		try {
			Query createQuery = em.createQuery("Select u from CheckInDetails u where u.vehicleId.vehicleId=:vehicleId and u.inTheParking = true", CheckInDetails.class);
			createQuery.setParameter("vehicleId", vehicleId);
			createQuery.setHint("javax.persistence.cache.storeMode", "REFRESH");
			Object singleResult2 = createQuery.getSingleResult();
			if (singleResult2 != null) {
				CheckInDetails singleResult = (CheckInDetails) singleResult2;
				if (!singleResult.getInTheParking()) {
					return null;
				}
				return singleResult;
			} 
			return null;
		} catch (NoResultException e) {
			return null;
		} finally {
			em.close();
		}
	}

	public Vector<CheckInDetails> getCheckInCars() {
		EntityManager em = getEntityManager();
		try {
			Query createQuery = em.createQuery("Select u from CheckInDetails u where u.vehicleId.vehicleId is not null AND u.inTheParking = true", CheckInDetails.class);
			createQuery.setHint("javax.persistence.cache.storeMode", "REFRESH");
			Object singleResult = createQuery.getResultList();
			if (singleResult != null) {
				return (Vector<CheckInDetails>) singleResult;
			} 
			return null;
		} catch (NoResultException e) {
			return null;
		} finally {
			em.close();
		}
	}

	public Vector<CheckInDetails> getCheckInPeople() {
		EntityManager em = getEntityManager();
		try {
			Query createQuery = em.createQuery("Select u from CheckInDetails u where u.personId.personId is not null AND u.inTheParking = true", CheckInDetails.class);
			createQuery.setHint("javax.persistence.cache.storeMode", "REFRESH");
			Object singleResult = createQuery.getResultList();
			if (singleResult != null) {
				return (Vector<CheckInDetails>) singleResult;
			} 
			return null;
		} catch (NoResultException e) {
			return null;
		} finally {
			em.close();
		}
	}
	
	
    
}

package com.mrk.hdcp.web.filters;

import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mrk.hdcp.web.entites.User;

/**
 * Servlet Filter implementation class UserFilter
 */
@WebFilter("/*")
public class UserFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public UserFilter() {
	}

	/**‰
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		HttpSession session = request.getSession(false);
		User user = (session != null) ? (User) session.getAttribute("loggedInUser") : null;

		if (user == null 
			&& !request.getRequestURI().endsWith("index.xhtml") 
			&& !request.getRequestURI().contains("javax.faces.resource")
			&& !request.getRequestURI().contains("resources/images")) {
			response.sendRedirect(request.getServletContext().getContextPath() +"/index.xhtml"); // No logged-in user found, so redirect to index page.
		} else {
			chain.doFilter(req, res); // Logged-in user found, so just continue request.
		}
	}

	
}

package com.mrk.hdcp.web.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.mrk.hdcp.web.bean.UsersMB;
import com.mrk.hdcp.web.entites.User;

@FacesConverter(forClass=User.class)
public class UserConverter implements Converter{

	@Inject
	private UsersMB userMB;
	@Override
	public Object getAsObject(FacesContext context, UIComponent component,String value) {
		return userMB.findUser(Integer.parseInt(value));
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,Object value) {
		return ((User)value).getUserId()+"";
	}
	
}
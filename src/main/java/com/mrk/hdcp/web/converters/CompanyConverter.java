package com.mrk.hdcp.web.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.mrk.hdcp.web.daos.CompaniesJpaController;
import com.mrk.hdcp.web.entites.Company;

@FacesConverter(forClass=Company.class)
public class CompanyConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,String value) {
		return new CompaniesJpaController().findCompanies(Integer.parseInt(value));
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,Object value) {
		return ((Company)value).getCompanyId()+"";
	}
	
}
package com.mrk.hdcp.web.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.mrk.hdcp.web.daos.CompaniesJpaController;
import com.mrk.hdcp.web.daos.PersonsJpaController;
import com.mrk.hdcp.web.entites.Company;
import com.mrk.hdcp.web.entites.Person;

@FacesConverter(forClass=Person.class)
public class PersonsConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,String value) {
		return new PersonsJpaController().findPerson(Integer.parseInt(value));
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,Object value) {
		return ((Person)value).getPersonId()+"";
	}
	
}
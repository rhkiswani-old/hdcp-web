/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mrk.hdcp.web.entites;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author mkiswani
 */
@Entity
@Table(name = "vehicles")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Vehicle.findAll", query = "SELECT v FROM Vehicle v"),
		@NamedQuery(name = "Vehicle.findByVehicleId", query = "SELECT v FROM Vehicle v WHERE v.vehicleId = :vehicleId"),
		@NamedQuery(name = "Vehicle.findByVehicleType", query = "SELECT v FROM Vehicle v WHERE v.vehicleType = :vehicleType"),
		@NamedQuery(name = "Vehicle.findByVehicleModel", query = "SELECT v FROM Vehicle v WHERE v.vehicleModel = :vehicleModel"),
		@NamedQuery(name = "Vehicle.findByVehiclePlateNumber", query = "SELECT v FROM Vehicle v WHERE v.vehiclePlateNumber = :vehiclePlateNumber"),
		@NamedQuery(name = "Vehicle.findByVehiclePermementId", query = "SELECT v FROM Vehicle v WHERE v.vehiclePermementId = :vehiclePermementId"),
		@NamedQuery(name = "Vehicle.findByIncludeGenerator", query = "SELECT v FROM Vehicle v WHERE v.includeGenerator = :includeGenerator") })
public class Vehicle implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "vehicle_id")
	private Integer vehicleId;
	@Column(name = "vehicle_type")
	private String vehicleType;
	@Column(name = "vehicle_model")
	private String vehicleModel;
	@Column(name = "vehicle_plate_number")
	private String vehiclePlateNumber;
	@Column(name = "vehicle_permement_id")
	private String vehiclePermementId;
	@Column(name = "include_generator")
	private Boolean includeGenerator;
	@Lob
	@Column(name = "vehicle")
	private byte[] vehicle;
	@OneToMany(mappedBy = "vehicleId")
	private Collection<CheckInDetails> checkInDetailsCollection;
	@OneToMany(mappedBy = "vehicleId")
	private Collection<CheckOutDetails> checkOutDetailsCollection;
	@JoinColumn(name = "company_id", referencedColumnName = "company_id")
	@ManyToOne
	private Company companyId;

	public Vehicle() {
	}

	public Vehicle(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}

	public Integer getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public String getVehiclePlateNumber() {
		return vehiclePlateNumber;
	}

	public void setVehiclePlateNumber(String vehiclePlateNumber) {
		this.vehiclePlateNumber = vehiclePlateNumber;
	}

	public String getVehiclePermementId() {
		return vehiclePermementId;
	}

	public void setVehiclePermementId(String vehiclePermementId) {
		this.vehiclePermementId = vehiclePermementId;
	}

	public Boolean getIncludeGenerator() {
		return includeGenerator;
	}

	public void setIncludeGenerator(Boolean includeGenerator) {
		this.includeGenerator = includeGenerator;
	}

	public byte[] getVehicle() {
		return vehicle;
	}

	public void setVehicle(byte[] vehicle) {
		this.vehicle = vehicle;
	}

	@XmlTransient
	public Collection<CheckInDetails> getCheckInDetailsCollection() {
		return checkInDetailsCollection;
	}

	public void setCheckInDetailsCollection(Collection<CheckInDetails> checkInDetailsCollection) {
		this.checkInDetailsCollection = checkInDetailsCollection;
	}

	@XmlTransient
	public Collection<CheckOutDetails> getCheckOutDetailsCollection() {
		return checkOutDetailsCollection;
	}

	public void setCheckOutDetailsCollection(Collection<CheckOutDetails> checkOutDetailsCollection) {
		this.checkOutDetailsCollection = checkOutDetailsCollection;
	}

	public Company getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Company companyId) {
		this.companyId = companyId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (vehicleId != null ? vehicleId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Vehicle)) {
			return false;
		}
		Vehicle other = (Vehicle) object;
		if ((this.vehicleId == null && other.vehicleId != null) || (this.vehicleId != null && !this.vehicleId.equals(other.vehicleId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.mrk.hdcp.web.entites.Vehicle[ vehicleId=" + vehicleId + " ]";
	}

	public StreamedContent getImage() throws IOException {
		if (vehicle != null) {
			return new DefaultStreamedContent(new ByteArrayInputStream(vehicle));
		}
		return null;
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mrk.hdcp.web.entites;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author mkiswani
 */
@Entity
@Table(name = "persons")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Person.findAll", query = "SELECT p FROM Person p"),
    @NamedQuery(name = "Person.findByPersonId", query = "SELECT p FROM Person p WHERE p.personId = :personId"),
    @NamedQuery(name = "Person.findByPersonName", query = "SELECT p FROM Person p WHERE p.personName = :personName"),
    @NamedQuery(name = "Person.findByPersonTel", query = "SELECT p FROM Person p WHERE p.personTel = :personTel"),
    @NamedQuery(name = "Person.findByPersonAddress", query = "SELECT p FROM Person p WHERE p.personAddress = :personAddress"),
    @NamedQuery(name = "Person.findByPersonPhotoId", query = "SELECT p FROM Person p WHERE p.personPhotoId = :personAge")})
public class Person implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "person_id")
    private Integer personId;
    @Basic(optional = false)
    @Column(name = "person_name")
    private String personName;
    @Basic(optional = false)
    @Lob
    @Column(name = "person_picture")
    private byte[] personPicture;
    @Column(name = "person_tel")
    private String personTel;
    @Column(name = "person_address")
    private String personAddress;
    @Column(name = "person_age")
    private Integer personPhotoId;
    @OneToMany(mappedBy = "personId")
    private Collection<CheckInDetails> checkInDetailsCollection;
    @OneToMany(mappedBy = "personId")
    private Collection<CheckOutDetails> checkOutDetailsCollection;
    @JoinColumn(name = "company_id", referencedColumnName = "company_id")
    @ManyToOne(optional = false)
    private Company companyId;

    public Person() {
    }

    public Person(Integer personId) {
        this.personId = personId;
    }

    public Person(Integer personId, String personName, byte[] personPicture) {
        this.personId = personId;
        this.personName = personName;
        this.personPicture = personPicture;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public byte[] getPersonPicture() {
        return personPicture;
    }

    public void setPersonPicture(byte[] personPicture) {
        this.personPicture = personPicture;
    }

    public String getPersonTel() {
        return personTel;
    }

    public void setPersonTel(String personTel) {
        this.personTel = personTel;
    }

    public String getPersonAddress() {
        return personAddress;
    }

    public void setPersonAddress(String personAddress) {
        this.personAddress = personAddress;
    }


    public Integer getPersonPhotoId() {
		return personPhotoId;
	}

	public void setPersonPhotoId(Integer personPhotoId) {
		this.personPhotoId = personPhotoId;
	}

	@XmlTransient
    public Collection<CheckInDetails> getCheckInDetailsCollection() {
        return checkInDetailsCollection;
    }

    public void setCheckInDetailsCollection(Collection<CheckInDetails> checkInDetailsCollection) {
        this.checkInDetailsCollection = checkInDetailsCollection;
    }

    @XmlTransient
    public Collection<CheckOutDetails> getCheckOutDetailsCollection() {
        return checkOutDetailsCollection;
    }

    public void setCheckOutDetailsCollection(Collection<CheckOutDetails> checkOutDetailsCollection) {
        this.checkOutDetailsCollection = checkOutDetailsCollection;
    }

    public Company getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Company companyId) {
        this.companyId = companyId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personId != null ? personId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Person)) {
            return false;
        }
        Person other = (Person) object;
        if ((this.personId == null && other.personId != null) || (this.personId != null && !this.personId.equals(other.personId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return personName;
    }
    public StreamedContent getImage() throws IOException {
		if (personPicture != null) {
			return new DefaultStreamedContent(new ByteArrayInputStream(personPicture));
		}
		return null;
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mrk.hdcp.web.entites;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import com.mrk.hdcp.web.daos.CheckInDetailsJpaController;

/**
 *
 * @author mkiswani
 */
@Entity
@Table(name = "check_out_details")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CheckOutDetails.findAll", query = "SELECT c FROM CheckOutDetails c"),
    @NamedQuery(name = "CheckOutDetails.findByCheckOutId", query = "SELECT c FROM CheckOutDetails c WHERE c.checkOutId = :checkOutId"),
    @NamedQuery(name = "CheckOutDetails.findByCheckOutDate", query = "SELECT c FROM CheckOutDetails c WHERE c.checkOutDate = :checkOutDate")})
public class CheckOutDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "check_out_id")
    private Integer checkOutId;
    @Column(name = "check_out_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkOutDate;
    @Lob
    @Column(name = "notes")
    private String notes;
    @JoinColumn(name = "vehicle_id", referencedColumnName = "vehicle_id")
    @ManyToOne
    private Vehicle vehicleId;
    @JoinColumn(name = "person_id", referencedColumnName = "person_id")
    @ManyToOne
    private Person personId;
    
    @Column(name = "checkin_id")
    private Integer checkInId;

    public CheckOutDetails() {
    }

    public CheckOutDetails(Integer checkOutId) {
        this.checkOutId = checkOutId;
    }

    public Integer getCheckOutId() {
        return checkOutId;
    }

    public void setCheckOutId(Integer checkOutId) {
        this.checkOutId = checkOutId;
    }

    public Date getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(Date checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Vehicle getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Vehicle vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Person getPersonId() {
        return personId;
    }

    public void setPersonId(Person personId) {
        this.personId = personId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (checkOutId != null ? checkOutId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CheckOutDetails)) {
            return false;
        }
        CheckOutDetails other = (CheckOutDetails) object;
        if ((this.checkOutId == null && other.checkOutId != null) || (this.checkOutId != null && !this.checkOutId.equals(other.checkOutId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mrk.hdcp.web.entites.CheckOutDetails[ checkOutId=" + checkOutId + " ]";
    }
    
    public Integer getCheckInId() {
		return checkInId;
	}

	public void setCheckInId(Integer checkInId) {
		this.checkInId = checkInId;
	}

	public CheckInDetails getCheckinDetails(){
    	if(getCheckInId() == null || getCheckInId() == 0){
    		return null;
    	}
    	
    	return new CheckInDetailsJpaController().findCheckInDetails(checkInId);
    }
}

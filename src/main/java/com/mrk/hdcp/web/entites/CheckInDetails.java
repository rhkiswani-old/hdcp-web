/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mrk.hdcp.web.entites;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mkiswani
 */
@Entity
@Table(name = "check_in_details")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CheckInDetails.findAll", query = "SELECT c FROM CheckInDetails c"),
    @NamedQuery(name = "CheckInDetails.findByCheckInId", query = "SELECT c FROM CheckInDetails c WHERE c.checkInId = :checkInId"),
    @NamedQuery(name = "CheckInDetails.findByCheckInDate", query = "SELECT c FROM CheckInDetails c WHERE c.checkInDate = :checkInDate"),
    @NamedQuery(name = "CheckInDetails.findByInTheParking", query = "SELECT c FROM CheckInDetails c WHERE c.inTheParking = :inTheParking")})
public class CheckInDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "check_in_id")
    private Integer checkInId;
    @Column(name = "check_in_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkInDate;
    @Column(name = "in_the_parking")
    private Boolean inTheParking;
    @Lob
    @Column(name = "notes")
    private String notes;
    @JoinColumn(name = "person_id", referencedColumnName = "person_id")
    @ManyToOne
    private Person personId;
    @JoinColumn(name = "vehicle_id", referencedColumnName = "vehicle_id")
    @ManyToOne
    private Vehicle vehicleId;

    public CheckInDetails() {
    }

    public CheckInDetails(Integer checkInId) {
        this.checkInId = checkInId;
    }

    public Integer getCheckInId() {
        return checkInId;
    }

    public void setCheckInId(Integer checkInId) {
        this.checkInId = checkInId;
    }

    public Date getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(Date checkInDate) {
        this.checkInDate = checkInDate;
    }

    public Boolean getInTheParking() {
        return inTheParking;
    }

    public void setInTheParking(Boolean inTheParking) {
        this.inTheParking = inTheParking;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Person getPersonId() {
        return personId;
    }

    public void setPersonId(Person personId) {
        this.personId = personId;
    }

    public Vehicle getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Vehicle vehicleId) {
        this.vehicleId = vehicleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (checkInId != null ? checkInId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CheckInDetails)) {
            return false;
        }
        CheckInDetails other = (CheckInDetails) object;
        if ((this.checkInId == null && other.checkInId != null) || (this.checkInId != null && !this.checkInId.equals(other.checkInId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mrk.hdcp.web.entites.CheckInDetails[ checkInId=" + checkInId + " ]";
    }
    
}
